# jroff

## Usage

(**TODO**)

## Contributing

For regular development, you only need (**TODO**)

Then, clone this repository:

```console
git clone ...
cd jroff
```

You can now build your local copy and run the tests:

```console
make all
```

### TODOs

- [ ] Data structure

### Links

- [Jison resources](http://bl.ocks.org/nolanlawson/6fd6848cf990582556e4)
- [Action features ($$, $1, etc) ](https://www.gnu.org/software/bison/manual/bison.html#Action-Features)
- [Jison debugger](http://nolanlawson.github.io/jison-debugger/)
- [About macros](http://www.schweikhardt.net/man_page_howto.html#q5)
- [About the `an` macro](http://linux.die.net/man/7/man)
- [About the `doc` macro](https://www.dragonflybsd.org/cgi/web-man?command=mdoc&section=7)
- [Troff spec](http://cm.bell-labs.com/sys/doc/troff.pdf)
- [Jison features](https://gist.github.com/GerHobbelt/6400988)
- [New features in Jison 0.3](https://gist.github.com/1659274)

## License

All the code contained in this repository, unless explicitly stated, is
licensed under MIT license.

A copy of the license can be found inside the [LICENSE](LICENSE) file.
