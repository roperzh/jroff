%%

\s+                         /* skip whitespace */
^\.|\n\s*\.                 return "COMMAND"
\n                          return "NEWLINE"
TH                          return "TH"
SH                          return "SH"
B                           return "B"
BI                          return "BI"
BR                          return "BR"
I                           return "I"
IB                          return "IB"
RB                          return "RB"
SB                          return "SB"
SM                          return "SM"
RS                          return "INCREASE_INDENT"
RE|LP|PP|P                  return "RESET_INDENT"
[a-zA-Z]+                   return "TEXT"
<<EOF>>                     return "EOF"

%%
