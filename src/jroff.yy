%start root

%ebnf

%{
  var currentIndent = "11%";
  var prevIndent = currentIndent;
  var preamble = "";
  var postamble = "";
  var result;
%}

%%

root
  : statement* EOF
    %{
      result = preamble + joinArray($1) + postamble + "\n";
      preamble = postamble = "";
      return result;
    }%
  ;

statement
  : COMMAND macro* NEWLINE
    -> joinArray($2)
  | NEWLINE TEXT*
    -> generateTag("p", $1)
  ;

macro
  : TH TEXT*
    -> generateTag("title", $2)
  | SH TEXT*
    %{
      preamble += generateTag("h1", $2);
      preamble += generateTag("a", $2);
      $$ = generateTag("h2", $2);
    }%
  | B TEXT*
    -> generateTag("p>b", $2, { indent: true })
  | BI TEXT*
    -> generateAlternTag("i", "b", $2, { indent: true })
  | BR TEXT*
    -> generateAlternTag("span", "b", $2, { indent: true })
  | I TEXT*
    -> generateTag("p>i", $2, { indent: true })
  | IB TEXT*
    -> generateAlternTag("b", "i", $2, { indent: true })
  | RB TEXT*
    -> generateAlternTag("b", "span", $2, { indent: true })
  | SB TEXT*
    -> generateAlternTag("span", "small", $2, { indent: true })
  | SM TEXT*
    -> generateTag("small", $2, { indent: true })
  | RESET_INDENT TEXT*
    -> resetIndent()
  | INCREASE_INDENT TEXT
    -> increaseIndent($2)
  ;

%%
