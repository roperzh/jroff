// -------------------------------------------
//   Helper functions
// -------------------------------------------

var generateTag = function(name, content, properties) {
  var tags = name.split(">"),
    i = -1,
    openingTags = "",
    closingTags = "",
    content = joinArray(content);

  while (tags[++i]) {
    openingTags += "<" + tags[i] + ">";
  }

  while (tags[--i]) {
    closingTags += "</" + tags[i] + ">";
  }

  return openingTags + content + closingTags;
}

var generateAlternTag = function(firstTag, secondTag, content, wrapperTag) {
  var wrapperTag = wrapperTag || "p";
  i = -1,
    result = "",
    currentTag = firstTag;

  while (content[++i]) {
    currentTag = currentTag === firstTag ? secondTag : firstTag;
    result += generateTag(currentTag, content[i]);
  }

  return generateTag(wrapperTag, result);
}

var resetIndent = function() {
  currentIndent = prevIndent;
}

var increaseIndent = function(increaseAmount) {
  var increaseAmount = parseInt(increaseAmount) || 0.5;

  prevIndent = currentIndent;
  currentIndent = currentIndent * increaseAmount;
}

var joinArray = function(arr) {
  return arr.join(" ");
}
