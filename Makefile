JISON=./node_modules/.bin/jison
LIBRARY=jroff

all: build test

build:
	cat src/$(LIBRARY).yy src/$(LIBRARY).js > src/$(LIBRARY).yy.temp
	$(JISON) src/$(LIBRARY).yy.temp src/$(LIBRARY).l
	mv $(LIBRARY).js build/$(LIBRARY).js
	rm -rf src/$(LIBRARY).yy.temp

test:
	node tests/all-tests.js

.PHONY: build
