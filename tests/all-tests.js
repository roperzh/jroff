var fs = require("fs"),
    path = require("path"),
    assert = require("assert"),
    parser = require("../build/jroff").parser;

// positive test cases
var posdir = path.join(__dirname, "positive-cases");
var ppath = fs.readdirSync(posdir);
ppath.forEach(function(f, i, e) {
  if (f.match(/groff$/)) {
    exports["test positive-cases/" + f] = function() {
      var parsed = parser.parse(fs.readFileSync(path.join(posdir, f), "utf8"));
      var a = fs.readFileSync(path.join(posdir, f), "utf8");
      console.log("parsed", a);
      console.log(parser.parse(a));
      var schema = fs.readFileSync(path.join(posdir, f.replace(/groff$/, 'html')), "utf8");
      assert.deepEqual(parsed, schema);
    };
  }
});

// negative test cases
// var negdir = path.join(__dirname, "negative_cases");
// var npath = fs.readdirSync(negdir);
// npath.forEach(function (f, i, e) {
//     if (f.match(/groff$/)) {
//         exports["test negative_cases/"+f] = function () {
//             assert["throws"](function () { parser.parse(fs.readFileSync(path.join(negdir, f), "utf8")); });
//         };
//     }
// });

if (require.main === module)
    require("test").run(exports);
